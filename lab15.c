#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct {
  int ra, telefone;
  char nome[100];
} Aluno;

typedef struct {
  int armazenado;
  int capacidade;
  Aluno *alunos;
} Base;

/* Funcao: criar_base
 *
 * Inicializa a base ja com a capacidade.
 *
 * Parametros:
 *   base: ponteiro para a base
 *   n: quantidade maxima de alunos
 */
void criar_base(Base *base, int n) {
    base->capacidade = n;
    base->armazenado = 0;
    base->alunos = (Aluno*) malloc(n*sizeof(Aluno));
    printf("Base criada.\n");
    return;
}

/* Funcao: buscar
 *
 * Parametros:
 *   base: ponteiro para a base
 *   ra: numero do RA
 *
 * Retorno:
 *   Indice do registro com RA no vetor de alunos
 *   -1 caso contrario.
 */
int buscar(Base *base, int ra) {
    int i;
    
    for(i=0; i<base->armazenado; i++)
        if(base->alunos[i].ra == ra)
            return i;
        
    return -1;
}

/* Funcao: imprimir
 *
 * Parametros:
 *   base: ponteiro para a base
 *   ra: numero do RA
 */
void imprimir(Base *base, int ra) {
    int i;
    
    for(i=0; i<base->armazenado; i++)
        if(base->alunos[i].ra == ra){
            printf("%d - %d - %s\n",base->alunos[i].ra,
                                    base->alunos[i].telefone,
                                    base->alunos[i].nome);
            return;
        }
    
    printf("Aluno %d nao encontrado.\n",ra);
    return;
}

/* Funcoes: adicionar
 *
 * Inclui um registro sem permitir RAs duplicados.
 * O quantidade de alunos deve ser atualizada.
 *
 * Parametros:
 *   base: ponteiro para a base
 *   ra: numero do RA
 *   telefone: numero do telefone
 *   nome: string com o nome
 */
void adicionar(Base *base, int ra, int telefone, char *nome) {
    int i;
    
    for(i=0; i<base->armazenado; i++)
        if(base->alunos[i].ra == ra){
            
            base->alunos[i].ra = ra;
            base->alunos[i].telefone = telefone;
            strcpy(base->alunos[i].nome,nome);
            
            printf("Alterado: %d - %d - %s\n",base->alunos[i].ra,
                                    base->alunos[i].telefone,
                                    base->alunos[i].nome);
            return;
        }
    
    if(i == base->capacidade){
        printf("Erro: base cheia.\n");
        return;
    }
    
    base->alunos[i].ra = ra;
    base->alunos[i].telefone = telefone;
    strcpy(base->alunos[i].nome,nome);
    base->armazenado++;
    
    printf("Adicionado: %d - %d - %s\n",base->alunos[i].ra,
                            base->alunos[i].telefone,
                            base->alunos[i].nome);
    return;
}

/* Funcoes: remover
 *
 * Remove um registro se o ra estiver presente.
 * O quantidade de registro deve ser atualizada.
 *
 * Parametros:
 *   base: ponteiro para a base
 *   ra: numero do RA
 */
void remover(Base *base, int ra) {
    int i;
    
    for(i=0; i<base->armazenado; i++)
        if(base->alunos[i].ra == ra){
            
            while(i < base->armazenado - 1){
                base->alunos[i] = base->alunos[i+1];
                i++;
            }
            
            base->armazenado--;
            printf("Aluno %d removido.\n",ra);
            return;
        }
        
    printf("Aluno %d nao encontrado.\n",ra);
    return;
}

/* Funcao: liberar_base
 *
 * Libera a memoria de todos alunos da base.
 * Deve deixar a base com capacidade e quantidade armazenada igual a zero
 * e o ponteiro para alunos igual a NULL.
 *
 * Parametros:
 *   base: ponteiro para a base
 */
void liberar_base(Base *base) {
    free(base->alunos); 
    base->alunos = NULL;
    base->capacidade = base->armazenado = 0;
    return;
}
